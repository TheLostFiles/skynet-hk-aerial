#include<iostream> // #include<iostream>
// empty space
using namespace std; // using namespace for my cout and endl
int location; // int for the actual location
int targetLocationPrediction; // int for the guessing of the location
int searchGridHighNumber; // int for what the high number is
int searchGridLowNumber; // int for what the low number is
// empty space
int main() // main function where the program starts
{// bracket boi
	searchGridHighNumber = 64; // sets the high number to 64
	searchGridLowNumber = 1; // sets the low number to 1
// empty space
	cout << "Target location Generated on a 8x8 grid..." << endl; //talking
	location = rand() % 64 + 1; // makes a random number
	cout << "The Target is located on Number "<< location <<"..." << endl; // talking
	cout << "The Aerial AI will now find out where the Target is Located..." << endl; // talking
// empty space
	do // start of a do while
	{// bracket boi
		targetLocationPrediction = ((searchGridHighNumber - searchGridLowNumber) / 2) + searchGridLowNumber; // magic maths things
		cout << "\n"; // next line
		cout << "The drone has tried at location: "; // talking
		cout << targetLocationPrediction; // next line
// empty space
		cout << "\n"; // next line
		if(location < targetLocationPrediction) // checks if the location is less then the prediction 
		{// bracket boi
			searchGridHighNumber = targetLocationPrediction; // changes the high number
		}// bracket boi
// empty space
		if (location > targetLocationPrediction) // checks if the location is higher then the prediction 
		{// bracket boi
			searchGridLowNumber = targetLocationPrediction; // changes the low number 
		}// bracket boi
		// empty space
	} while (targetLocationPrediction != location); // makes sure it doesn't so anything until it has the right number 
// empty space
	cout << "\nThe Drone has found the Target on Number "<< location <<"..." << endl; // talking
// empty space
	cout << "The Target has been Eliminated...\n" << endl; // talking
// empty space
	system("pause"); // pause 
// empty space
	main(); // re runs the main function 
}// bracket boi